# thermal-treatment-open-data

This repository contains open data obtained from bench-scale, thermal treatment
experiments.

### Contents

* [Installation](#installation)
* [Usage](#usage)
* [Licensing](#licensing)
* [Datasets](#datasets)

## Installation

There are two installation options, depending on how much data you are
interested in. To obtain the entire set, ensure [Git](https://git-scm.com) is
installed and [clone the
repository](https://help.github.com/en/articles/cloning-a-repository) into your
preferred location:

```sh
git clone https://gitlab.com/jdrsch/thermal-treatment-open-data /path/to/location
```

If you only want to download individual files, you can do so using `curl` on
the command line:

```sh
curl https:://gitlab.com/jdrsch/datasets/<file>
```

where `<file>` is an individual file, e.g., `lttt-pcp-mechanism-probe-product-yields.csv`.

## Usage

The files in this repository use the comma-separated values (`*.csv`) format.
File comments are provided at the start of each file and are prepended with
`#`. To mine the data, simply open up your favorite computing environment and
run the following for a given file path, `/path/to/file.csv`:

#### R

```r
# Using base R
d <- read.csv('/path/to/file.csv', comment = '#', stringsAsFactors = FALSE)

# Using readr
library(readr)
d <- read_csv('/path/to/file.csv', comment = '#')

# Using data.table
library(data.table)
d <- fread('/path/to/file.csv')
```

#### Python

```python
# Using pandas
import numpy as np
import pandas as pd
d = pd.read_csv('/path/to/file.csv', comment = '#')
```

#### MATLAB

```matlab
% Using statistical
d = readtable('/path/to/file.csv')
```

## Licensing

This repository is licensed under an [Open Database License
(ODbL)](https://www.opendatacommons.org/licenses/odbl/1.0/). If you modify,
redistribute, or publish content with our datasets, please make sure you adhere
to the following, as per the ODbL agreement:

1. **Citation**: You must cite the papers from which the dataset originated. Citation information is provided in two places: the comment header in each `.csv` file and in the [Datasets](#datasets) section below.
2. **Share alike**: If you adapt or redistribute the data, you must openly
publish some version of the resulting dataset.

## Datasets

Descriptions, citation information, and schema are provided for each dataset
below.

* [**`lttt-pcp-mechanism-probe-product-yields.csv`**](https://gitlab.com/jdrsch/thermal-treatment-open-data/datasets/lttt-pcp-mechanism-probe-yields.csv)

  * **Description**: Product yields during low-temperature (200-400&deg;C)
    thermal treatment of pentachlorophenol (PCP).

  * **Citation information**:

     >R. J. Davis, L. E. Katz, H. M. Liljestrand, Evidence for multiple
     >removal pathways in low-temperature (200-400&deg;C) thermal treatment of
     >pentachlorophenol
  * **Schema**:

    | field                | data type | description                              |
    | :------------------- | :-------- | :--------------------------------------- |
    | id                   | integer   | primary key                              |
    | product              | character | product name                             |
    | product_cid          | integer   | product PubChem CID                      |
    | abbreviation         | character | product abbreviation                     |
    | associated_mechanism | character | mechanism associated with product        |
    | n_chorines           | integer   | number of chlorine groups in product     |
    | n_hydroxyls          | integer   | number of hydroxyl groups in product     |
    | n_carbonyls          | integer   | number of carbonyl groups in product     |
    | ring_oxidation_state | character | ring oxidation state                     |
    | n_ortho              | integer   | number of ortho substituents in product  |
    | n_meta               | integer   | number of meta substituents in product   |
    | n_para               | integer   | number of para substituents in product   |
    | sweep                | character | experimental sweep gas                   |
    | flow_rate            | numeric   | sweep gas flow rate                      |
    | min_hold             | numeric   | experimental hold time in minutes        |
    | celsius_hold         | numeric   | experimental hold temperature in Celsius |
    | product_phase        | character | product phase                            |
    | yield                | numeric   | mean product yield                       |
    | yield_sd             | numeric   | product yield standard deviation         |
    | yield_se             | numeric   | product yield standard error             |
    | yield_ci             | numeric   | product yield confidence invervals (99%) |
